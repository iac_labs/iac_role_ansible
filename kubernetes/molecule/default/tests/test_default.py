
import testinfra.utils.ansible_runner
import subprocess
import os
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


#Verificando binarios e arquivos

def test_hosts_file(host):

    for name in (
     "/usr/bin/kubeadm",
     "/usr/bin/kubectl",
     "/usr/bin/kubelet",
     "/etc/docker/daemon.json",
    ):

        f = host.file(name).exists

# Verificando se docker está rodando


def test_if_container_is_executing(host):
    cmd = host.run("docker container run -d --name molecule_nginx nginx")

    assert cmd.rc == 0

def test_if_container_is_runing(host):
   container = host.docker("molecule_nginx")

   assert container.is_running


def test_if_container_is_removed(host):
    cmd = host.run("docker container rm -f molecule_nginx")

    assert cmd.rc == 0

# Testando kubeadm

def test_if_kubeadm_is_executing(host):
    cmd = host.run("kubeadm config images pull")

    assert cmd.rc == 0
