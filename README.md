# iac_role_ansible

Essa role tem o objetivo de instalar os itens necessários para subir um cluster kubernetes com container runtime Docker.

## Requisitos

- Instale o [Python 3](https://www.python.org/downloads/)

```sh
python3 -m venv .ansible
source .ansible/bin/activate
pip install --upgrade pip
python3 -m pip install -r requirements.txt
```

## Testando a role:

```sh
cd kubernetes
molecule test
```

- Testando rapidamente após modificação:

```sh
molecule create
molecule converge
molecule verify

```

- Após acabar seus testes:

```sh
molecule destroy
```
